# O - Matching

~~*搬运 by DOlaBMOon*~~

## Problem Statement

There are $N$ men and $N$ women, both numbered $1, 2, ..., N$. 

Taro is trying to make $N$ pairs, each consisting of a man and a woman who are compatible. Here, each man and each woman must belong to exactly one pair. 

Find the number of ways in which Taro can make $N$ pairs, modulo $10^9 + 7$

有 $N$ 个男人和 $N$ 个女人, 分别都编号为 $1...N$. 

Taro 要尝试去将男人和女人两两 make_pair, 使得每个男人都被分配到了一个和自己 compatible 的女人. 

计算方案数, 对 $10^9 + 7$ 取模. 

## Constraints

* All values in input are integers. 
* $1 \leq N \leq 21$
* $a_{i, j}$ is $0$ or $1$

## Input

Input is given from Standard Input in the following format: 

| Input Format |
| --- |
| $N$ <br> $a_{1, 1}$ $...$ $a_{1, N}$ <br> $:$ <br> $a_{N, 1}$ $...$ $a_{N, N}$ |

## Output

Print the number of ways in which Taro can make $N$ pairs, modulo $10^9 + 7$. 

## Sample(s)

| Sample Input 1 |
| --- |
| 3 <br> 0 1 1 <br> 1 0 1 <br> 1 1 1 |

| Sample Output 1 |
| --- |
| 3 |

There are three ways to make pairs, as follows ($(i, j)$ denotes a pair of Man $i$ and Woman $j$): 

* $(1, 2), (2, 1), (3, 3)$
* $(1, 2), (2, 3), (3, 1)$
* $(1, 3), (2, 1), (3, 2)$

