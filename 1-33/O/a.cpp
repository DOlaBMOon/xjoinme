#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define Bitcnt __builtin_popcount

const int N=21;
const int MS=(1<<21)+10;
const int MOD=1e9+7;

void SU(int &x,int y)
{
	((x+=y)>=MOD)&&(x-=MOD);
}

int n,dp[MS],S,g[N];

int main()
{
	dp[0]=1;
	scanf("%d",&n);
	S=(1<<n)-1;
	for(int i=0;i<n;++i)
	{
		for(int j=0;j<n;++j)
			scanf("%d",g+j);
		for(int k=0;k<=S;++k)if(dp[k]&&Bitcnt(k)==i)
		{
			for(int j=0;j<n;++j)if(g[j])
				SU(dp[k^(1<<j)],dp[k]);
		}
	}
	printf("%d",dp[S]);
	return 0;
}
