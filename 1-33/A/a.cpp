#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define Int128 __int128
#define LL long long

const int N=1e5+10;

int n;
LL h[N];
Int128 dp[N];

void _Print(Int128 x)
{
	if(!x)
		return;
	_Print(x/10);
	putchar('0'+x%10);
}

void Print(Int128 x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	_Print(x);
}

int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
		scanf("%lld",h+i);
	for(int i=2;i<=n;++i)
	{
		dp[i]=dp[i-1]+abs(h[i]-h[i-1]);
		if(i>1)
			GetMin(dp[i],dp[i-2]+abs(h[i]-h[i-2]));
	}
	Print(dp[n]);
	return 0;
}
