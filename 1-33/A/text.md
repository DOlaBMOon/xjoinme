# A - Frog 1

~~*搬运 by DOlaBMOon*~~

## Problem Statement

There are $N$ stones, numbered $1, 2, ..., N$. For each $i (1 \leq i \leq N)$, the height of Stone $i$ is $h_i$. 


There is a frog who is initially on Stone $1$. He will repeat the following action some number of times to reach Stone $N$:

* If the frog is currently on Stone $i$, jump to Stone $i + 1$ or Stone $i + 2$. Here, a cost of $|h_i - h_j|$ is incurred, where $j$ is the stone to land on. 

Find the minimum possible total cost incurred before the frog reaches Stone $N$. 

有 $N$ 块石头, 编号为 $1, 2, ..., N$. 对于每个 $i (1 \leq i \leq N)$, 石头 $i$ 的高度为 $h_i$. 

有一只蛤一开始在石头 $1$. 他将重复一下操作一些次数以便到达石头 $N$. 

* 如果这只蛤现在在石头 $i$, 它有能力跳到石头 $i + 1$ 或石头 $i + 2$. 费用为 $|h_i - h_j|$, $j = i + 1\ or\ i + 2$. 

计算最小费用~ 

## Constraints

* All values in input are integers. 
* $2 \leq N \leq 10^5$
* $1 \leq h_i \leq 10^{18}$

## Input

Input is given from Standard Input in the following format: 

| Input Format |
| :--- |
| $N$ <br> $h_1$ $h_2$ $...$ $h_N$ |

## Output

Print the minimum possible total cost incurred. 

## Sample(s)

| Sample Input 1 |
| --- |
| 4 <br> 10 30 40 20 |

| Sample Ouput 1 |
| --- |
| 30 |

If we follow the path $1 \rightarrow 2 \rightarrow 4$, the total cost in incurred would be $30$. 


