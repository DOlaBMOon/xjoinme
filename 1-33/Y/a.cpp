#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define LL long long
#define Pir pair<int,int>
#define fir first
#define sec second

const int N=3333;
const int MX=2e5+10;
const int MOD=1e9+7;

void SU(int &x,int y)
{
	((x+=y)>=MOD)&&(x-=MOD);
}

void SD(int &x,int y)
{
	((x-=y)<0)&&(x+=MOD);
}

int D(int x,int y)
{
	return ((x-=y)<0)?(x+MOD):x;
}

int Qpow(int x,int y=MOD-2)
{
	int res=1;
	for(;y;y>>=1,x=(LL)x*x%MOD)if(y&1)
		res=(LL)res*x%MOD;
	return res;
}

int h,w,n;
Pir p[N];
int dp[N];

int jc[MX],njc[MX];

int C(int n,int m)
{
	return (LL)jc[n]*njc[m]%MOD*njc[n-m]%MOD;
}

void Prework()
{
	int mx=h+w;
	jc[0]=1;
	for(int i=1;i<=mx;++i)
		jc[i]=(LL)jc[i-1]*i%MOD;
	njc[mx]=Qpow(jc[mx]);
	for(int i=mx-1;i>=0;--i)
		njc[i]=(LL)njc[i+1]*(i+1)%MOD;
}

int Calc(const Pir &a,const Pir &b)
{
	int x=b.fir-a.fir,y=b.sec-a.sec;
	return C(x+y,x);
}

int main()
{
	scanf("%d%d%d",&h,&w,&n);
	Prework();
	for(int i=1;i<=n;++i)
		scanf("%d%d",&p[i].fir,&p[i].sec);
	//for(int i=1;i<=n;++i)
	//	for(int j=1;j<i;++j)
	//		assert(p[i]!=p[j])
	sort(p+1,p+n+1);
	int ans=C(h+w-2,h-1);
	for(int i=1;i<=n;++i)
	{
		dp[i]=MOD-(LL)Calc((Pir){1,1},p[i]);
		for(int j=1;j<i;++j)if(p[j].sec<=p[i].sec)
			SD(dp[i],(LL)dp[j]*Calc(p[j],p[i])%MOD);
		SU(ans,(LL)dp[i]*Calc(p[i],(Pir){h,w})%MOD);
	}
	printf("%d",ans);
	return 0;
}
