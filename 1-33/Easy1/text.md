# 方格取数

## Problem Statement

在一个 $n \times n$ 的方格里, 每个格子里都有一个正整数. 从中取出若干数, 使得任意两个取出的数所在格子没有公共边, 且取出的数的总和尽量大. 

## Constraints

* $n \leq 30$

## Input

第一行一个数 $n$,接下来 $n$ 行每行 $n$ 个数描述一个方阵. 

## Ouput

最大和. 

## Sample(s)

| Sample Input 1 |
| --- |
| 2 <br> 1 2 <br> 3 5 |

| Sample Output 1 |
| --- |
| 6 |


