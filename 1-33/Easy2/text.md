# Windy 数

## Problem Statement

Windy 定义了一种 Windy 数: 不含前导零且相邻两个数字之差至少为 $2$ 的正整数被称为 Windy 数. 

Windy 想知道, 在 $A$ 和 $B$ 之间，包括 $A$ 和 $B$, 总共有多少个 Windy 数? 

## Input

一行两个数, 分别为 $A,B$. 

## Output

输出一个整数, 表示答案. 

## Constraints

* $1 \le A \le B \le 2 \times 10^9$. 

## Sample(s)

#### Sample Input 1

```plain
1 10
```
#### Sample Output 1

```plain
9
```
#### Sample Input 2

```plain
25 50
```
#### Sample Output 2

```plain
20
```



