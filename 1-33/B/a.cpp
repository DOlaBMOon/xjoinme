#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<queue>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

const int _B=1e6;

char Nc()
{
	static char buf[_B],*p=buf,*q=buf;
	return ((p==q)&&(q=buf+fread(buf,1,_B,stdin))==(p=buf))?0:*(p++);
}

int Read()
{
	char ch;
	while(!isdigit(ch=Nc()));
	int sum=ch-48;
	while(isdigit(ch=Nc()))
		sum=sum*10+ch-48;
	return sum;
}

#define LL long long

const int N=1e6+10;
const int H=1e5+10;
const LL INF=0x3f3f3f3f3f3f3f3f;

struct Heap
{

	priority_queue<LL,vector<LL>,greater<LL> > P,Q;

	void Push(LL x)
	{
		P.push(x);
	}

	void Del(LL x)
	{
		Q.push(x);
	}

	LL Top()
	{
		while(!Q.empty()&&P.top()==Q.top())
		{
			P.pop();
			Q.pop();
		}
		return P.empty()?INF:P.top();
	}

};

LL tr0[H<<2],tr1[H<<2];
Heap hp0[H<<2],hp1[H<<2];

void Insert(int p,LL w0,LL w1,int o=1,int l=1,int r=H)
{
	if(l==r)
	{
		hp0[o].Push(w0);
		hp1[o].Push(w1);
		GetMin(tr0[o],w0);
		GetMin(tr1[o],w1);
		return;
	}
	int mid=(l+r)>>1,lc=o<<1,rc=lc^1;
	if(p<=mid)
		Insert(p,w0,w1,lc,l,mid);
	else
		Insert(p,w0,w1,rc,mid+1,r);
	tr0[o]=min(tr0[lc],tr0[rc]);
	tr1[o]=min(tr1[lc],tr1[rc]);
}

void Del(int p,LL w0,LL w1,int o=1,int l=1,int r=H)
{
	if(l==r)
	{
		hp0[o].Del(w0);
		hp1[o].Del(w1);
		tr0[o]=hp0[o].Top();
		tr1[o]=hp1[o].Top();
		return;
	}
	int mid=(l+r)>>1,lc=o<<1,rc=lc^1;
	if(p<=mid)
		Del(p,w0,w1,lc,l,mid);
	else
		Del(p,w0,w1,rc,mid+1,r);
	tr0[o]=min(tr0[lc],tr0[rc]);
	tr1[o]=min(tr1[lc],tr1[rc]);
}

LL Ask(int ql,int qr,LL *tr,int o=1,int l=1,int r=H)
{
	if(ql<=l&&r<=qr)
		return tr[o];
	int mid=(l+r)>>1,lc=o<<1,rc=lc^1;
	LL res=INF;
	if(ql<=mid)
		GetMin(res,Ask(ql,qr,tr,lc,l,mid));
	if(qr>mid)
		GetMin(res,Ask(ql,qr,tr,rc,mid+1,r));
	return res;
}

int n,ki,h[N];
LL dp[N];

void Insert(int x)
{
	Insert(h[x],dp[x]-h[x],dp[x]+h[x]);
}

void Del(int x)
{
	Del(h[x],dp[x]-h[x],dp[x]+h[x]);
}

int main()
{
	memset(tr0,0x3f,sizeof tr0);
	memset(tr1,0x3f,sizeof tr1);
	//scanf("%d%d",&n,&ki);
	n=Read();
	ki=Read();
	for(int i=1;i<=n;++i)
		//scanf("%d",h+i);
		h[i]=Read();
	for(int i=1;i<=n;++i)
	{
		if(i>1)
			dp[i]=min(Ask(1,h[i],tr0)+h[i],Ask(h[i],H,tr1)-h[i]);
		if(i<n)
		{
			Insert(i);
			if(i>ki)
				Del(i-ki);
		}
	}
	printf("%lld",dp[n]);
	return 0;
}
