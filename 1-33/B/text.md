# B - Frog 2

~~*搬运 by DOlaBMOon*~~

## Problem Statement

在 *A - Frog 1* 中, 蛤可以往后跳 $2$ 格, 现在它可以往后跳 $K$ 格啦! 

(数据范围也有所不同了呢...)

(话说在上一题中 C++ 的 long long 好像是存不下的, 怎么办呢, XJOI 跑在 64 位的机器上哦...~~__int128~~!)

## Constraints

* All values in input are integers. 
* $2 \leq N, K \leq 10^6$
* $1 \leq h_i \leq 10^5$

## Input

Input is given from Standard Input in the following format: 

| Input Format |
| :--- |
| $N$ $K$ <br> $h_1$ $h_2$ $...$ $h_N$ |

## Output

Print the minimum possible total cost incurred. 

## Sample(s)

| Sample Input 1 |
| --- |
| 4 2 <br> 10 30 40 20 |

| Sample Ouput 1 |
| --- |
| 30 |

If we follow the path $1 \rightarrow 2 \rightarrow 4$, the total cost in incurred would be $30$. (笑


