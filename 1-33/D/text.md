# D - Knapsack 0

~~*造 by DOlaBMOon*~~

## Problem Statement

qwe 有 $N$ 个木棒, 长度分别为 $l_1, l_2, ..., l_N$. 

她想要选择一些木棒, 首尾相接, 问有多少种长度可以最终生成. 

## Constraints

* All values in input are integers. 
* $1 \leq N, l_i \leq 10^3$

## Input

Input is given from Standard Input in the following format: 

| Input Format |
| --- |
| $N$ <br> $l_1$ $l_2$ $...$ $l_N$ |

## Output

Print the answer. 

## Sample(s)

| Sample Input 1 |
| --- |
| 4 <br> 1 2 4 8 |

| Sample Ouput 1|
| --- |
| 16 |

特别地, $0$ 也算作一种木棒长度. 

