import random

f=0

def Out(n):
	print(n,file=f)
	for i in range(0,n):
		print(random.randint(1,n),end='',file=f)
		if i<n-1:
			print(' ',end='',file=f)

for i in range(0,10):
	n=int(pow(10,3/(10-i)))
	f=open('input'+str(i),'w')
	Out(n)
