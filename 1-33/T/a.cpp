#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

const int N=3333;
const int MOD=1e9+7;

void SU(int &x,int y)
{
	((x+=y)>=MOD)&&(x-=MOD);
}

int n;
char s[N];
int f[N];

int main()
{
	scanf("%d%s",&n,s+2);
	f[1]=1;
	for(int i=2;i<=n;++i)
	{
		if(s[i]=='>')
		{
			for(int j=2;j<i;++j)
				SU(f[j],f[j-1]);
			for(int j=i;j>=2;--j)
				f[j]=f[j-1];
			f[1]=0;
		}
		else
		{
			for(int j=i-2;j>=1;--j)
				SU(f[j],f[j+1]);
		}
	}
	int ans=0;
	for(int i=1;i<=n;++i)
		SU(ans,f[i]);
	printf("%d",ans);
	return 0;
}
