import random

f=0

def Out(n):
	print(n,file=f)
	for i in range(0,n-1):
		print(random.choice(['<','>']),end='',file=f)

for i in range(0,10):
	n=3*int(pow(10,3/(10-i)))
	f=open('input'+str(i),'w')
	Out(n)
