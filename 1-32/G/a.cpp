#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4.1,sse4.2,avx,avx2,popcnt,tune=native")
#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

const int N=4444;
const int INF=0x3f3f3f3f;
const int _B=1e6;

char Nc()
{
	static char buf[_B],*p=buf,*q=buf;
	return (p==q&&(q=buf+fread(buf,1,_B,stdin))==(p=buf))?0:*(p++);
}

int Read()
{
	char ch;
	while(!isdigit(ch=Nc()));
	int res=ch-48;
	for(;isdigit(ch=Nc());res=res*10-48+ch);
	return res;
}

int n,T,s[N][N],w[N][N],f[N],g[N];

void Solve(int ql,int qr,int l,int r)
{
	if(ql>qr)
		return;
	int mid=(ql+qr)>>1,p;
	g[mid]=INF;
	for(int i=l;i<=r&&i<mid;++i)if(GetMin(g[mid],f[i]+w[mid][i+1]))
		p=i;
	Solve(ql,mid-1,l,p);
	Solve(mid+1,qr,p,r);
}

void Song()
{
	Solve(1,n,0,n-1);
	swap(f,g);
}

int main()
{
	//scanf("%d%d",&n,&T);
	n=Read();
	T=Read();
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=n;++j)
		{
			//scanf("%d",&s[i][j]);
			s[i][j]=Read()+s[i][j-1]+s[i-1][j]-s[i-1][j-1];
		}
	}
	for(int i=1;i<=n;++i)
		for(int j=i;j<=n;++j)
			w[j][i]=s[j][j]-s[i-1][j]-s[j][i-1]+s[i-1][i-1];
	memset(f,0x3f,sizeof f);
	f[0]=0;
	while(T--)
		Song();
	printf("%d",f[n]/2);
	return 0;
}
