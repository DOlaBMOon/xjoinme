# G - Ciel and Gondolas

~~*搬运 by DOlaBMOon*~~ 

| 233 |
| :---: |
| time limit per test: 2 seconds |
| memory limit per test: 512 megabytes |
| input: standard input |
| output: standard output |

## Problem Statement

Fox Ciel is in the Amusement Park. And now she is in a queue in front of the Ferris wheel. There are n people (or foxes more precisely) in the queue: we use first people to refer one at the head of the queue, and n-th people to refer the last one in the queue.

There will be k gondolas, and the way we allocate gondolas looks like this:

* When the first gondolas come, the q1 people in head of the queue go into the gondolas. 
* Then when the second gondolas come, the q2 people in head of the remain queue go into the gondolas. 
    ...
* The remain qk people go into the last (k-th) gondolas. 

Note that q1, q2, ..., qk must be positive. You can get from the statement that $\sum\limits_{i = 1}^k q_i = n$ and  $q_i > 0$.

You know, people don't want to stay with strangers in the gondolas, so your task is to find an optimal allocation way (that is find an optimal sequence q) to make people happy. For every pair of people i and j, there exists a value uij denotes a level of unfamiliar. You can assume uij = uji for all i, j (1 ≤ i, j ≤ n) and uii = 0 for all i (1 ≤ i ≤ n). Then an unfamiliar value of a gondolas is the sum of the levels of unfamiliar between any pair of people that is into the gondolas.

A total unfamiliar value is the sum of unfamiliar values for all gondolas. Help Fox Ciel to find the minimal possible total unfamiliar value for some optimal allocation.

给一个序列, 您需要将其分为 $k$ 段, 设其中一段为 $[l, r]$, 则这一段的代价为 $\sum\limits_{i \in [l, r], j \in [l, r]} u_{i, j}$. 

计算总代价和的最小值. 

## Input

The first line contains two integers n and k (1 ≤ n ≤ 4000 and 1 ≤ k ≤ min(n, 800)) — the number of people in the queue and the number of gondolas. Each of the following n lines contains n integers — matrix u, (0 ≤ uij ≤ 9, uij = uji and uii = 0).

Please, use fast input methods (for example, please use BufferedReader instead of Scanner for Java).

## Output

Print an integer — the minimal possible total unfamiliar value.

## Sample(s)

#### Sample Input 1

```plain
5 2
0 0 1 1 1
0 0 1 1 1
1 1 0 0 0
1 1 0 0 0
1 1 0 0 0
```

#### Sample Ouput 1

```plain
0
```

