import random as Rd
n=4000
k=800
print(n,k)
A=[[0]*n for i in range(n)];
for i in range(n):
	for j in range(n):
		if i<j:
			A[i][j]=Rd.randint(0,9)
		elif i>j:
			A[i][j]=A[j][i]
	print(' '.join([str(x) for x in A[i]]))
