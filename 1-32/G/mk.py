import random as Rd

f=0

def Out(n):
	k=Rd.randint(1,n)
	print(n,k,file=f)
	A=[[0]*n for i in range(n)];
	for i in range(n):
		for j in range(n):
			if i<j:
				A[i][j]=Rd.randint(0,9)
			elif i>j:
				A[i][j]=A[j][i]
		print(' '.join([str(x) for x in A[i]]),file=f)

for i in range(10):
	f=open('input'+str(i),'w')
	Out(4*int(10**(4/(10-i))))
