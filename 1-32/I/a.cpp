#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define int long long

const int N=23333;
const int INF=0x3f3f3f3f3f3f3f3f;

int n,ki,p[N],f[N],g[N],s[N],w[N];

int Cost(int l,int r)
{
	int t=s[r]-s[l-1],x=r-l+1;
	return w[r]-w[l-1]+2*(s[l-1]*x-t*(l-1))
		-x*p[r]+t
		-p[l-1]*(r-l)*(l-1)+(t-p[r])*(l-1)
		+p[r]*x*(n-r)-t*(n-r);
}

int tCost(int l,int r)
{
	int t=s[r]-s[l-1],x=r-l+1;
	return w[r]-w[l-1]+2*(s[l-1]*x-t*(l-1))
		-p[l-1]*x*(l-1)+t*(l-1)
		+p[r]*x*(n-r)-t*(n-r);
}

void Solve(int ql,int qr,int l,int r)
{
	if(ql>qr)
		return;
	int mid=(ql+qr)>>1,p=-1;
	g[mid]=INF;
	for(int i=l;i<=r&&i<mid;++i)if(GetMin(g[mid],f[i]+Cost(i+1,mid)))
		p=i;
	Solve(ql,mid-1,l,p);
	Solve(mid+1,qr,p,r); }

void Song()
{
	Solve(1,n,0,n-1);
	swap(f,g);
}

signed main()
{
	while(~scanf("%lld%lld",&n,&ki))
	{
		for(int i=1;i<=n;++i)
		{
			scanf("%lld",p+i);
			s[i]=s[i-1]+p[i];
			w[i]=w[i-1]+2*(p[i]*(i-1)-s[i-1]);
		}
		memset(f,0x3f,sizeof f);
		f[0]=0;
		for(;ki--;Song());
		int ans=f[n];
		for(int i=0;i<n;++i)
			GetMin(ans,f[i]+tCost(i+1,n));
		printf("%lld\n",ans);
	}
	return 0;
}
