import random as Rd

f=0

def Out(n):
	k=min(200,Rd.randint(0,n))
	print(n,k,file=f)
	print(' '.join(str(x) for x in sorted(Rd.sample(range(10**7),n))),file=f)

for i in range(10):
	f=open('input'+str(i),'w')
	Out(max(2,10**int(4/(10-i))))
