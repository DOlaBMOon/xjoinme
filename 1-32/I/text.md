# I - 标志重捕法

~~*搬运 by DOlaBMOon*~~

tl = 2s
ml = 512MB

## Problem Statement

There are $n$ stations on the road, which are located at $x_1 ... x_n (x_1 < x_2< ... < x_n)$. 

Your task is to choose $k$ locations to put signs (signs could be put at anywhere, not necessary to be at a station). Let $c(i, j)$ be the distance from the $i^{th}$ station to the closest sign between station $i$ and station $j$. $c(i, j) = |x_i - x_j|$ if there's no sign between them. Find the optimal solution of the sign locations to minimizd $\sum\limits_{i \not= j} c(i, j)$. 

有 $n$ 个点 $x_1 ... x_n (x_1 < x_2< ... < x_n)$, 您需要选择 $k$ 个位置放上标志 (**注意**: 标志不一定放在整点上). 

$$
c(i, j) = \left\{
\begin{aligned}
&|x_i - x_j|, \text{两点之间没有标志} \\
&min\{|x_i - k|\}, k \in \text{所有的在两点间的标志的位置集合}
\end{aligned}
\right.
$$

 找到一种放置方案使得 $\sum\limits_{i \not= j} c(i, j)$ 最小. 

## Input

The input will consist of multiple test cases. Each case begins with two integers $n$ and $k$. $(0 \leq k \leq 200, 2 \leq n \leq 10000)$

The following line contains $n$ integers, $x_1 ... x_n$. $(0 \leq x_1 < x_2< ... < x_n \leq 10^7)$. 

## Output

For each test case print one integer, the minimal value of $\sum\limits_{i \not= j} c(i, j)$. 

## Sample(s)

#### Sample Input 1

```plain
4 0
1 2 3 4
4 1
1 2 3 4
```

#### Sample Output 1

```plain
20
11
```

