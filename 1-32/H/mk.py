import random as Rd

f=0

def Out(n):
	k=min(300,Rd.randint(1,n))
	print(n,k,file=f)
	print(' '.join([str(Rd.randint(1,10000)) for x in range(n)]),file=f)

for i in range(10):
	f=open('input'+str(i),'w')
	Out(3*int(10**(3/(10-i))))
