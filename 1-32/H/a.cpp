#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4.1,sse4.2,avx,avx2,popcnt,tune=native")
#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

const int N=4444;
const int INF=0x3f3f3f3f;
const int _B=1e6;

int n,T,p[N],s[N],f[N],g[N],w[N][N];

void Solve(int ql,int qr,int l,int r)
{
	if(ql>qr)
		return;
	int mid=(ql+qr)>>1,p=0;
	g[mid]=INF;
	for(int i=l;i<=r&&i<mid;++i)if(GetMin(g[mid],f[i]+w[mid][i+1]))
		p=i;
	Solve(ql,mid-1,l,p);
	Solve(mid+1,qr,p,r);
}

void Song()
{
	Solve(1,n,0,n-1);
	swap(f,g);
}

int main()
{
	scanf("%d%d",&n,&T);
	for(int i=1;i<=n;++i)
	{
		scanf("%d",p+i);
		s[i]=s[i-1]+p[i];
	}
	sort(p+1,p+n+1);
	for(int i=1;i<=n;++i)
	{
		for(int j=i,t;j<=n;++j)
		{
			t=(i+j)>>1;
			w[j][i]=p[t]*((t-i+1)-(j-t))-(s[t]-s[i-1])+(s[j]-s[t]);
			//cout<<i<<","<<j<<":"<<w[j][i]<<endl;
		}
	}
	memset(f,0x3f,sizeof f);
	f[0]=0;
	while(T--)
	{
		Song();
		//Divhim();
		//for(int i=1;i<=n;++i)
		//	cout<<f[i]<<" ";
		//cout<<endl;
	}
	printf("%d",f[n]);
	return 0;
}
