#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define LL long long

const int N=12;
const int MS=(1<<(N+1))+10;

int n,m,mp[N][N],S;
LL f[MS],g[MS];

void Work()
{
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;++i)
		for(int j=0;j<m;++j)
			scanf("%d",&mp[i][j]);
	S=(1<<(m+1))-1;
	memset(f,0,sizeof(*f)*(S+1));
	f[0]=1;
	for(int i=0;i<n;++i)
	{
		for(int j=0;j<m;++j)
		{
			memset(g,0,sizeof(*g)*(S+1));
			for(int k=0;k<=S;++k)if(f[k])
			{
				//cout<<i<<","<<j<<":"<<k<<endl;
				int a=(k>>j)&1,b=(k>>(j+1))&1;
				if(mp[i][j])
				{
					if(a^b)
					{
						g[k^(a<<j)^(b<<(j+1))^(1<<j)]+=f[k];
						g[k^(a<<j)^(b<<(j+1))^(1<<(j+1))]+=f[k];
					}
					else
						g[k^(1<<j)^(1<<(j+1))]+=f[k];
				}
				else
				{
					if(a||b)
						continue;
					g[k]+=f[k];
				}
			}
			swap(f,g);
		}
		memset(g,0,sizeof(*g)*(S+1));
		for(int k=S>>1;k>=0;--k)
			g[k<<1]=f[k];
		swap(f,g);
	}
	printf("%lld\n",f[0]);
}

int main()
{
	int T;
	for(scanf("%d",&T);T--;Work());
	return 0;
}
