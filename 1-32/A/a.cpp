#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define LL long long

const int N=1e6+10;
const LL INF=0x3f3f3f3f3f3f3f3f;

int n;LL c;
LL dp[N];
LL ki[N<<4],bi[N<<4];

LL Ask(int p,int o=1,int l=1,int r=N)
{
	if(!ki[o])
		return INF;
	if(l==r)
		return ki[o]*p+bi[o];
	int mid=(l+r)>>1,lc=o<<1,rc=lc^1;
	if(p<=mid)
		return min(ki[o]*p+bi[o],Ask(p,lc,l,mid));
	else
		return min(ki[o]*p+bi[o],Ask(p,rc,mid+1,r));
}

void Update(LL k,LL b,int o=1,int l=1,int r=N)
{
	if(!ki[o])
	{
		ki[o]=k;
		bi[o]=b;
		return;
	}
	if(ki[o]==k)
	{
		GetMin(bi[o],b);
		return;
	}
	int mid=(l+r)>>1,lc=o<<1,rc=lc^1;
	if((k*mid+b)<(ki[o]*mid+bi[o]))
	{
		swap(k,ki[o]);
		swap(b,bi[o]);
	}
	if(l==r)
		return;
	if((k*l+b)<(ki[o]*l+bi[o]))
		Update(k,b,lc,l,mid);
	else
		Update(k,b,rc,mid+1,r);
}

int main()
{
	scanf("%d%lld",&n,&c);
	for(int i=1;i<=n;++i)
	{
		LL h;
		scanf("%lld",&h);
		if(1<i)
			dp[i]=Ask(h)+h*h;
		Update(-2*h,dp[i]+c+h*h);
	}
	printf("%lld",dp[n]);
	return 0;
}
