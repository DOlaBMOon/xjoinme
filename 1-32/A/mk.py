import random

f=0
p=list(range(1,10**6))

def Out(n):
	print(n,random.randint(1,10**12),file=f)
	random.shuffle(p)
	h=p[0:n]
	h.sort()
	for i in range(0,n):
		print(h[i],end='',file=f)
		if i<n-1:
			print(' ',end='',file=f)

for i in range(0,10):
	print(i)
	n=int(2*pow(10,5/(10-i)))
	f=open('input'+str(i),'w')
	Out(n)
