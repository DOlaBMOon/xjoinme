#include<cstdio>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>

using namespace std;

#define Whats(x) cout<<#x<<" is "<<(x)<<endl
#define Divhim() cout<<">>>>>>>>>>>>>>>"<<endl
#define Divher() cout<<"<<<<<<<<<<<<<<<"<<endl
#define Oops() cout<<"!!!!!!!!!!!!!!!"<<endl

template<typename T> bool GetMin(T &a,T b)
{
	return ((a<=b)?false:(a=b,true));
}

template<typename T> bool GetMax(T &a,T b)
{
	return ((a>=b)?false:(a=b,true));
}

/*
	 -<Unlimited Blade Works>-
 */

#define int long long
#define Int __int128
#define Pir pair<int,int>
#define fir first
#define sec second

const int N=1e5+10;

int n,T,s[N],mx;
int f[N],ki[N],bi[N],cnt[N],h,t;

bool Check(int k0,int b0,int k1,int b1,int k2,int b2,int x,int y)
{
#define NP pair<long double,int>
	return (NP){(long double)(b2-b0)*(k0-k1),-x}<=(NP){(long double)(b1-b0)*(k0-k2),-y};
#undef NP
}

bool Song(int c)
{
	cnt[h=t=0]=-1;
	for(int i=1,nk,nb,ncnt;i<=n;++i)
	{
		for(;h<t&&(Pir){s[i]*ki[h]+bi[h],-cnt[h]}<=(Pir){s[i]*ki[h+1]+bi[h+1],-cnt[h+1]};++h);
		f[i]=s[i]*ki[h]+bi[h];
		ncnt=cnt[h]+1;
		nk=s[i];
		nb=f[i]-s[i]*s[i]+c;
		for(;h<=t&&ki[t]==nk&&bi[t]<=nb;--t);
		for(;h<t&&Check(ki[t-1],bi[t-1],ki[t],bi[t],nk,nb,cnt[t],i);--t);
		if(h<=t&&ki[t]==nk&&nb<bi[t])
			continue;
		ki[++t]=nk;
		bi[t]=nb;
		cnt[t]=ncnt;
	}
	return cnt[t]<=T;
}

signed main()
{
	scanf("%lld%lld",&n,&T);
	for(int i=1;i<=n;++i)
	{
		scanf("%lld",s+i);
		mx+=s[i]*s[i-1];
		s[i]+=s[i-1];
	}
	int left=0,mid,right=mx,res=mx;
	while(left<=right)
	{
		if(Song(-(mid=(left+right)>>1)))
			right=(res=mid)-1;
		else
			left=mid+1;
	}
	Song(-res);
	printf("%lld",f[n]+T*res);
	return 0;
}
